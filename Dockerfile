FROM rocker/r-ver:4.0.0

## Install linux packages
RUN apt-get update -y && apt-get full-upgrade -y && apt-get install -y \
    sudo \
    gdebi-core \
    pandoc \
    pandoc-citeproc \
    libjq-dev \
    libcurl4-openssl-dev \
    libcairo2-dev \
    libxt-dev \
    libssl-dev \
    libudunits2-dev \
    libxml2 \
    libxml2-dev \
    unixodbc-dev \
    libudunits2-0 \
    libudunits2-dev\
    libjq-dev \
    libprotobuf-dev \
    protobuf-compiler \
    libmagick++-dev \
    libmariadbclient-dev \
    libssl-dev \
    libcairo2-dev \
    libc6 \
    libodbc1 \
    xtail \
    curl \
    wget

## Install Java
RUN apt-get update && apt-get -y --no-install-recommends install \ 
    cmake \
    default-jdk \
    default-jre \
    && sudo R CMD javareconf


## Geo dependencies
RUN apt-get update && apt-get -y --no-install-recommends install \ 
    r-cran-rjava \
    default-libmysqlclient-dev \
    libmysqlclient-dev \
    libgeos-dev \
    gdal-bin \
    libgdal-dev \ 
    libproj-dev \
    tcl8.6-dev \
    tk8.6-dev
  
## Install MYSQL drivers/connectors 
RUN curl -L -o /mysql-connector-java-5.1.34.jar https://repo1.maven.org/maven2/mysql/mysql-connector-java/5.1.34/mysql-connector-java-5.1.34.jar
ENV CLASSPATH=/mysql-connector-java-5.1.34.jar:${CLASSPATH}

RUN apt-get update -qq && apt-get -y --no-install-recommends install curl \
    && apt-get install unzip \
    && curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip awscliv2.zip \
    && ./aws/install

    
## download and install R-Packages
# Spatial packages
RUN R -e "install.packages('raster', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('rgdal', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('stringr', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('sp', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('sf', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('ddpcr', dependencies = TRUE, repos='http://cran.rstudio.com/')"

# Connection packages
RUN R -e "install.packages('aws.s3', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('RMariaDB', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('DBI', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('aws.ec2metadata', dependencies = TRUE, repos='http://cran.rstudio.com/')"

# General packages
RUN R -e "install.packages('dplyr', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('tidyverse', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('devtools', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('cluster', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('readODS', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('data.table', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('REdaS', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('paws', dependencies = TRUE, repos='http://cran.rstudio.com/')"

# Machine learning packages
RUN R -e "install.packages('caretEnsemble', dependencies = TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('caret', dependencies = TRUE, repos='http://cran.rstudio.com/')" 


## Copy the code files into the image
COPY EV_Registration_Upload.R /usr/local/src/EV_Registration_Upload.R
COPY Download_R_base_script.R /usr/local/src/Download_R_base_script.R

## set permissions
RUN chmod 777 /usr/local/src/

## set the working direcotry
WORKDIR /usr/local/src/

## open port 3306 because of the database
EXPOSE 3306

## command as an R script the R-file
CMD ["Rscript", "/usr/local/src/Download_R_base_script.R"]